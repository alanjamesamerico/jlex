package br.com.jlex.core.scraping;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class DicioScrapingTest {
	
	private DicioScraping dicio = new DicioScraping();
	
	@Test
	public void testJsoup() throws IOException {
		
		String word = dicio.getWordSearch();
		String significance = dicio.getSignificance();
		System.out.println(word + "\n" + significance);
		
		ArrayList<String> contentSpans = (ArrayList<String>) dicio.getDescriptionsSearchedWord();
		for (String s : contentSpans) {
			System.out.println(s);
		}
	}
}
