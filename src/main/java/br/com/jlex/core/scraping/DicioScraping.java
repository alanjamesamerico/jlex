package br.com.jlex.core.scraping;

import static br.com.jlex.core.helper.TagHelper.H1;
import static br.com.jlex.core.helper.TagHelper.H2;
import static br.com.jlex.core.helper.TagHelper.P_ITEMPROP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Provides data scraping through the {@link Scraping} interface to the <a href="https://www.dicio.com.br/">
 * Dicio</a> website, obtaining information on the meaning of words in English.
 */
@Component
public class DicioScraping extends AbstractScraping {

	private final String URL_DICIO = "https://www.dicio.com.br/";
	
	private final String URL_IMAGE = "https://s.dicio.com.br/";
	
	public DicioScraping() {
		try {
			this.site = this.accessSite(URL_DICIO);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String getWordSearch() {
		return this.site.select(H1).first().text();
	}

	@Override
	public List<String> getDescriptionsSearchedWord() {
		ArrayList<String> contentSpans = new ArrayList<String>();
		this.site.select(P_ITEMPROP).forEach( span -> {
			for (int i = 0; i < span.getElementsByTag("span").size(); i++) {
				contentSpans.add(span.getElementsByTag("span").get(i).text());
			}
		});
		return contentSpans;
	}

	public String getSignificance() {
		return this.site.select(H2).first().text();
	}
	
	public String getUrlImage() {
		return this.URL_IMAGE + this.wordSearched + this.IMG_JPG;
	}
}
