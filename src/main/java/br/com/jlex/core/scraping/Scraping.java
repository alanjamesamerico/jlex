package br.com.jlex.core.scraping;

import java.io.IOException;
import java.util.List;

import org.jsoup.nodes.Document;

/**
 * Interface class for connection and manipulation of scraped data.
 */
public interface Scraping {
	
	/**
	 * Access the website that will be scraping data.
	 * @param url of the site
	 * @return a {@link Document} type connection
	 */
	Document accessSite(String url) throws IOException;
	
	
	/**
	 * Scrapes the website connected to the searched word.
	 * @return the wordSearched
	 */
	String getWordSearch();
	
	
	/**
	 * Scrapes the website connected to the searched word.
	 * @return description of the searched word
	 */
	String getDescriptionSearchedWord();
	
	
	/**
	 * scrapes from the connected site more than one description of the searched word
	 * @return descriptions list of the searched word
	 */
	List<String> getDescriptionsSearchedWord();
}
