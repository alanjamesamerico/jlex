package br.com.jlex.core.scraping;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Abstraction for classes that scrape data.
 */
public abstract class AbstractScraping implements Scraping {
	
	public Document site;
	
	public String wordSearched;
	
	public final String IMG_JPG = ".jpg";
	
	
	@Override
	public Document accessSite(String url) throws IOException {
		return Jsoup.connect(url).get();
	}
	
	@Override
	public String getWordSearch() {
		return null;
	}

	@Override
	public String getDescriptionSearchedWord() {
		return null;
	}

	@Override
	public List<String> getDescriptionsSearchedWord() {
		return null;
	}
}
