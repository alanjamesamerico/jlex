package br.com.jlex.core.scraping;

/**
 * Provides data scraping through the {@link Scraping} interface to the 
 * <a href="https://dictionary.cambridge.org/pt/">Cambridge Dictionary</a> website, obtaining 
 * information on the meaning of words in English.
 */
public class CambridgeDictionaryScraping /* extends AbstractScraping */ {
	
	
}
