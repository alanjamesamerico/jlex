package br.com.jlex.core.helper;

public class TagHelper {
	
	public static final String BODY  = "<body>";
	public static final String HEAD  = "<head>";
	public static final String TITLE = "<title>";
	public static final String SPAN  = "<span>";
	
	public static final String H1 = "h1";
	public static final String H2 = "h2";
	public static final String H3 = "h3";
	public static final String H7 = "h7";
	public static final String P  = "p";
	public static final String P_ITEMPROP  = "p[itemprop]";
}
