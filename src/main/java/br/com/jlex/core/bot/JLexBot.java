package br.com.jlex.core.bot;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import br.com.jlex.core.helper.BotResponseMessageHelper;
import br.com.jlex.core.helper.CommandsHelper;
import br.com.jlex.core.scraping.DicioScraping;

@PropertySource("classpath:application.properties")
public class JLexBot extends TelegramLongPollingBot {

	@Autowired
	private DicioScraping dicio;
	
	@Value("${bot.username}")
	private String botUsername;
	
	@Value("${bot.token}")
	private String token;
	
	
	@Override
	public void onUpdateReceived(Update update) {
		System.out.println("\t> Receved..");
		if (update.hasMessage() && update.getMessage().hasText()) {
			System.out.println("> Text: " + update.getMessage().getText());
	        this.handleIncomingMessage(update.getMessage());
	    } else {
	    	SendMessage message = new SendMessage() 
	    		.setChatId(update.getMessage().getChatId())
	            .setText("Desculpe, não entendi o que você digitou.\n Por favor, digite novamente =)");
			try {
				execute(message);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
	    }
	}
	
	@Override
	public String getBotUsername() {
		return this.botUsername;
	}

	@Override
	public String getBotToken() {
		return this.token;
	}
	
	public void handleIncomingMessage(Message message) {
		try {
			if (CommandsHelper.isStart(message.getText())) {
				sendWelcomeJlex(message);
			} else if (CommandsHelper.isStop(message.getText())) {
				sendGoodByeJlex(message);
			} else {
				sendResponseSearch(message);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void sendResponseSearch(Message message) throws TelegramApiException {
		
		if (isValideWordReceived(message)){
			SendPhoto sendPhoto = new SendPhoto();
			sendPhoto.setChatId(message.getChatId());
			sendPhoto.setPhoto(dicio.getUrlImage());
			
			try {
				
				this.execute(sendPhoto);
				Thread.sleep(1500);
				
			} catch (TelegramApiException e) {
				
				SendMessage msg = new SendMessage();
				msg.setChatId(message.getChatId());
				msg.setText(BotResponseMessageHelper.IMG_NOT_FOUND);
				
				this.execute(msg);
				
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try {
				
				SendMessage response = generateResponseMessage(message);
				
				this.execute(response);
				
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean isValideWordReceived(Message message) {
		
		String word = dicio.getWordSearch();

		if (!StringUtils.isEmpty(word)) {
			return true;
		} else {
			responseWordNotFound(message);
		}
		return false;
	}
	
	private void responseWordNotFound(Message message) {
		try {
			SendMessage msg = new SendMessage();
			msg.setChatId(message.getChatId());
			msg.setText(BotResponseMessageHelper.WORD_NOT_FOUND);
			this.execute(msg);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private SendMessage generateResponseMessage(Message message) {
		
 		SendMessage response = new SendMessage();
		String content = "";
		response.setChatId(message.getChatId());
		content = dicio.getWordSearch() +"\n---\n\n"+ 
				dicio.getSignificance() + "\n";
		
		List<String> descriptions = dicio.getDescriptionsSearchedWord();
		if (!descriptions.isEmpty()) {
			for (String description : descriptions) {
				content = content + "> " + description +"\n\n";
			}
			response.setText(content);
		} else {
			responseWordNotFound(message);
		}
		
		return response;
	}

	private void sendGoodByeJlex(Message message) {
		try {
			
			SendMessage responseMessage = new SendMessage();
			responseMessage.setChatId(message.getChatId());
			responseMessage.setText("Ei"+ message.getFrom().getFirstName() +", não vai embora =/");
			
			this.execute(responseMessage);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void sendWelcomeJlex(Message message) {
		try {
			SendMessage responseMessage = new SendMessage();
			responseMessage.setChatId(message.getChatId());
			responseMessage.setText("Ei " + getFullNameSender(message) +", "+ BotResponseMessageHelper.WELCOME);
			
			this.execute(responseMessage);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getFullNameSender(Message message) {
		boolean firstName = StringUtils.isNoneEmpty(message.getFrom().getFirstName());
		boolean lastName = StringUtils.isNoneEmpty(message.getFrom().getLastName());
		if (firstName && lastName) {
			return message.getFrom().getFirstName()+" "+ message.getFrom().getLastName();
		} else {
			return message.getFrom().getFirstName();
		}
	}
}
