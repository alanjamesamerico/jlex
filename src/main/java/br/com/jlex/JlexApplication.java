package br.com.jlex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class JlexApplication {

	public static void main(String[] args) {
		ApiContextInitializer.init();
		SpringApplication.run(JlexApplication.class, args);
	}
}
