# **JLex**

<h3>The Bot dictionary</h3>

<p> JLex is a robot dictionary that helps users to identify the meaning of words using Telegram.</p>

## How to use

To use the dictionary via Telegram you need:

1. Install the Telegram app from [Google Play](https://bit.ly/3anRItZ) or [Apple Store](https://apple.co/3eD4VCB).

2. Search for bot `@jaylexbot`.

![Image of Yaktocat](https://i.ibb.co/2PhdYWv/telegram-search.png)

3. Click start. The command `/star` will be sent automatically so the dictionary can be used.

4. Send the word you would like to know the meaning. Example: `alegria`.
